# GameJam

A simple **WIP** game create on GameJam during the [Turniej Trójgamiczny](https://t3g.pl) competition's
workshops.

# Contribution

Feel free to contribute if you wish! All types of isues and merge requests are welcome!
However I don't promise that I will develop this project on my hand ;-)
