using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Core;
public class ClockStatus : MonoBehaviour
{
    internal bool activated = false;
    internal bool usingClock = false;
    public void ReverseTime()
    {
        if(!activated)
            ChangeTime.AddTime(2);
        usingClock = true;
        activated = true;
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(.1f);
        usingClock = false;
    }
}
