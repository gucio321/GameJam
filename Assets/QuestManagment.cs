using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.UI.Quests;

namespace GameJam.Quests
{
    public class QuestManagment : MonoBehaviour
    {
        [SerializeField]
        List<GameObject> ThingsToTake;

        [SerializeField]
        string[] TextOnQuestLog;

        [SerializeField]
        private QuestWritter questWritter;

        int currentQuest = 0;


        private void Start()
        {
            FirstQuest();
        }
        public void FirstQuest()
        {
            currentQuest = 1;
            for (int i = 0; i < 3; i++)
            {
                questWritter.PushLine(TextOnQuestLog[i]);
            }
        }

        public void UpdateFirstQuest(GameObject gameo)
        {
            questWritter.EndLine(TextOnQuestLog[ThingsToTake.IndexOf(gameo)]);
        }


    }
}
