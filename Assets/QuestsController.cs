using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.Core
{
    public class QuestsController : MonoBehaviour
    {
        internal static int currentQuest = 0;

        [Header("Tutorial Quest Objects"), SerializeField]
        List<Transform> tutoQuestObjects;

        [Header("First Quest Objects"), SerializeField]
        List<Transform> fQuestObjects;

        [Header("Second Quest Objects"), SerializeField]
        List<Transform> sQuestObjects;

        [Header("Third Quest Objects"), SerializeField]
        List<Transform> tQuestObjects;

        private float number;

        private void Start()
        {
            ResetO();
            TutorialQuest();
        }

        public void ChangeQuest()
        {
            ResetO();
            if(currentQuest != 3)
                currentQuest++;
            switch (currentQuest)
            {
                case 1:
                    FirstQuest();
                    break;
                case 2:
                    SecondQuest();
                    break;
                case 3:
                    ThirdQuest();
                    break;
            }

        }

        private void ResetO()
        {
            foreach (Transform t in tutoQuestObjects)
            {
                t.gameObject.SetActive(false);
            }
            foreach (Transform item in fQuestObjects)
            {
                item.gameObject.SetActive(false);
            }
            foreach (Transform item in sQuestObjects)
            {
                item.gameObject.SetActive(false);
            }
            foreach (Transform item in tQuestObjects)
            {
                item.gameObject.SetActive(false);
            }
        }

        private void TutorialQuest()
        {
            number = tutoQuestObjects.Count;
            foreach (Transform item in tutoQuestObjects)
            {
                item.gameObject.SetActive(true);
            }
        }

        public void FirstQuest()
        {
            ChangeTime.StartTime();
            number = fQuestObjects.Count;   
            foreach (Transform item in fQuestObjects)
            {
                item.gameObject.SetActive(true);
            }
        }

        public void SecondQuest()
        {
            number = sQuestObjects.Count;
            foreach (Transform item in sQuestObjects)
            {
                item.gameObject.SetActive(true);
            }
        }

        private void ThirdQuest()
        {
            foreach (Transform item in tQuestObjects)
            {
                item.gameObject.SetActive(true);
            }
        }

        public void RemoveGameObject(Transform tobject)
        {
            switch (currentQuest)
            {
                case 0:
                    tutoQuestObjects.Remove(tobject);
                    break;
                case 1:
                    fQuestObjects.Remove(tobject);
                    break;
                case 2:
                    sQuestObjects.Remove(tobject);
                    break;
                case 3:
                    tQuestObjects.Remove(tobject);
                    break;

            }

        }

        public void CheckIfFInished()
        {
            
            switch (currentQuest)
            {
                case 0:
                    if (tutoQuestObjects.Count  == number / 2)
                        ChangeQuest();
                    break;

                case 1:
                    if (fQuestObjects.Count == number / 2)
                        ChangeQuest();
                    break;
                case 2:
                    if (sQuestObjects.Count == number / 2)
                        ChangeQuest();
                    break;
            }

        }
    }
}
