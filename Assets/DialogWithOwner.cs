using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DialogWithOwner : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    DialogueSystem dialogueSystem;

    [SerializeField]
    Canvas canvas;

    float timer = 5f;
    int licznik = 0;
    public bool work = false;

    private void Start()
    {
    }

    private void Update()
    {
        if (work)
        {
            if (timer < 0)
            {
                dialogueSystem.ShowDialogue();
                timer = 5f;
                licznik++;
            }
            if (licznik == 6)
            {
                SceneManager.LoadScene(3);
            }
            timer -= Time.deltaTime;
        }
    }
}
