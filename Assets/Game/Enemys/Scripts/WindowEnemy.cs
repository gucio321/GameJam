using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.AI.WindowEnemy
{
    public class WindowEnemy : MonoBehaviour
    {
        private bool waiting = false;
        private int DetectedPlayer = 0;

        private FieldOfView view;

        public Vector3[] EnemyPositions;

        [SerializeField]
        Light flashlight;

        private SkinnedMeshRenderer[] mesh;

        [SerializeField]
        private float unActiveTime = 1f;


        private float timer;

        [SerializeField]
        private float timerTime = 10f;



        private void Start()
        {
            view = GetComponent<FieldOfView>();
            mesh = GetComponentsInChildren<SkinnedMeshRenderer>();
            flashlight.enabled = true;
            timer = timerTime;
            ChangePosition();
        }


        private void Update()
        {
            if (!waiting && view.IsInField())
            {
                DetectedPlayer += 1;
                if (DetectedPlayer == 3)
                    GameOverController.GameOver();
                foreach(SkinnedMeshRenderer value in mesh)
                    value.enabled = false;
                flashlight.enabled = false;
                waiting = true;
                StartCoroutine(Wait());
            }
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                ChangePosition();
            }
        }

        private void ChangePosition()
        {
            transform.position = EnemyPositions[Random.Range(0, EnemyPositions.Length)];
            if (transform.position.x > 50f)
                transform.rotation = Quaternion.Euler(0, -90, 0);
            else if (transform.position.x < 50f && transform.position.x > 40f)
                transform.rotation = Quaternion.Euler(0, 180, 0);
            else
                transform.rotation = Quaternion.Euler(0, 90, 0);
            timer = timerTime;
        }

        private IEnumerator Wait()
        {
            yield return new WaitForSeconds(unActiveTime);
            transform.position = EnemyPositions[Random.Range(0, EnemyPositions.Length)];
            if (transform.position.x > 50f)
                transform.rotation = Quaternion.Euler(0, -90, 0);
            else if(transform.position.x < 50f && transform.position.x > 40f)
                transform.rotation = Quaternion.Euler(0, 180, 0);
            else
                transform.rotation = Quaternion.Euler(0, 90, 0);

            foreach (SkinnedMeshRenderer value in mesh)
                value.enabled = true;
            flashlight.enabled = true;
            waiting = false;
        }
        
    }
}
