using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJam.AI.WindowEnemy
{
    public class FieldOfView : MonoBehaviour
    {
        private LayerMask playerMask;
        private LayerMask obstructionMask;

        [Range(0, 50)]
        public float radius = 10;

        [Range(0, 180)]
        public float angle = 120;


        private void Start()
        {
            playerMask = LayerMask.GetMask("Player");
            obstructionMask = LayerMask.GetMask("Default");
        }

        public bool IsInField()
        {
            Collider[] rangeChecks = Physics.OverlapSphere(transform.position, radius, playerMask);
            if (rangeChecks.Length != 0)
            {
                Transform target = rangeChecks[0].transform;
                Vector3 directionToTarget = (target.position - transform.position).normalized;
                if (Vector3.Angle(transform.forward, directionToTarget) < angle / 2)
                {
                    float distanceToTarget = Vector3.Distance(target.position, transform.position);
                    if (!Physics.Raycast(transform.position, directionToTarget, distanceToTarget, obstructionMask))
                        return true;

                    return false;
                }
                return false;
            }
            return false;
        }
    }
}
