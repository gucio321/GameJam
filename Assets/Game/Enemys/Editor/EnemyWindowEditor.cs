using UnityEditor;
using UnityEngine;
using GameJam.AI.WindowEnemy;

[CustomEditor(typeof(WindowEnemy))]
public class EnemyWindowEditor : Editor
{
    SerializedProperty waypointsProp;
    private void OnEnable()
    {
        waypointsProp = serializedObject.FindProperty("EnemyPositions");
    }

    private void OnSceneGUI()
    {
        int numberOfWaypoints = waypointsProp.arraySize;
        for (int i = 0; i < numberOfWaypoints; i++)
        {
            SerializedProperty element = waypointsProp.GetArrayElementAtIndex(i);
            element.vector3Value = Handles.PositionHandle(element.vector3Value, Quaternion.identity);
        }
        serializedObject.ApplyModifiedProperties();
    }
}
