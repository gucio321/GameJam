using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace GameJam.Core
{
    public class ChangeTime : MonoBehaviour
    {
        [SerializeField, Tooltip("1 LengthofDay = 1 minut in real life")]
        private float LengthOfDay = 1;

        [SerializeField]
        private TMP_Text time;


        private static float GameTime;
        private static bool isPlaying = false;

        void Start()
        {
            GameTime = LengthOfDay * 60;
        }

        public static void AddTime(float duration)
        {
            GameTime += duration * 60f;
        }

        // Update is called once per frame
        void Update()
        {
            if(time != null)
                time.text = GameTime.ToString();
            if (isPlaying)
            {
                GameTime -= Time.deltaTime;
                if (GameTime < 0)
                {
                    GameOverController.GameOver();
                }
                Debug.Log(GameTime);
            }
        }
        public static void StartTime()
        {
            isPlaying = true;
        }
        public static void StopTime()
        {
            isPlaying = false;
        }

    }
}
