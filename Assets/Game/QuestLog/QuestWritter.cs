using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace GameJam.UI.Quests {
public class QuestWritter : MonoBehaviour
{
    [SerializeField]
    private TMP_Text textField;

        List<string> onBoard;
        private void Start()
        {
            onBoard = new List<string>();
        }
        public void PushLine(string line)
        {
            textField.text += ">" + line + "\n";
            onBoard.Add(line);
        }
        public void EndLine(string line)
        {
            Rebuild(line);
        }

        private void Rebuild(string line)
        {
            textField.text = "";
            foreach (string value in onBoard)
            {
                if(value == line)
                    textField.text += "<s>" + value + "</s>"+"\n";
                else
                    textField.text += ">" + value + "\n";
            }
        }

        public void Clear()
        {
            textField.text = "";
        }
   
}
}
