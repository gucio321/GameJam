using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class SceneLoader : MonoBehaviour
{
    [SerializeField]
    LoadingController loader;
    [SerializeField]
    TMP_Text loadingText;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(2);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            //Output the current progress
            loader.SetProgress(asyncOperation.progress);
            loadingText.text = "Postęp: " + (loader.CurrentProgress() * 100).ToString() + "%";
            // Check if the load has finished
            if (asyncOperation.progress >= .9f)
            {
                loader.FullProgress();
                //Change the Text to show the Scene is ready
                loadingText.text = "Press the space bar to continue";
                //Wait to you press the space key to activate the Scene
                if (Input.GetKeyDown(KeyCode.Space))
                    //Activate the Scene
                    asyncOperation.allowSceneActivation = true;
            }


            yield return null;
        }
    }
}
