using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemTypes : MonoBehaviour
{
    public ItemType itemTypes;
}

public enum ItemType { 
    Walizka,
    Buty,
    Noz,
    Chleb,
    Ser,
    Szynka,
    Maslo,
    Klucz,
    Bagaze
}
