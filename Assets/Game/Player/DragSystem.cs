using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJam.Core;

public class DragSystem : MonoBehaviour
{
    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private Transform slot;

    [SerializeField]
    private float pickingUpTime = .5f;

    
    private float picikingTime = 0;


    private DragableItem item = null;


    [SerializeField]
    private QuestsController controller;


    private void Start()
    {
        picikingTime = pickingUpTime;
    }

    private void Update()
    {

        if (Input.GetKey(KeyCode.E))
        {
            Ray ray = playerCamera.ViewportPointToRay(new Vector2(.5f, .5f));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 1.5f))
            {
                DragableItem itemToHand = hit.transform.GetComponent<DragableItem>();
                ItemTypes type = hit.transform.GetComponent<ItemTypes>();
                ClockStatus clock = hit.transform.GetComponent<ClockStatus>();
                
                if(hit.transform.tag == "WorkingObject")
                {
                    Debug.Log("dziala");
                    pickingUpTime -= Time.deltaTime;
                    if (pickingUpTime < 0)
                        controller.ChangeQuest();
                }

                if (itemToHand != null)
                {
                    pickingUpTime -= Time.deltaTime;
                    if (pickingUpTime < 0)
                        DragItem(itemToHand);
                }
                else if (this.item != null && type != null)
                {
                    pickingUpTime -= Time.deltaTime;
                    if (pickingUpTime < 0)
                        PlaceItem(type);
                }
                if(clock != null)
                {
                    clock.ReverseTime();
                }

            }
        }
        else
            pickingUpTime = picikingTime;
    }
        

    private void DragItem(DragableItem item)
    {
        item.gameObject.GetComponent<BoxCollider>().enabled = false;
        item.Rigidbody.isKinematic = true;
        item.Rigidbody.velocity = Vector3.zero;
        item.Rigidbody.angularVelocity = Vector3.zero;

        item.transform.SetParent(slot);
        item.transform.localRotation = Quaternion.identity;
        item.transform.localPosition = Vector3.zero;
        pickingUpTime = picikingTime;
        this.item = item;

    }
    private void PlaceItem(ItemTypes item)
    {
        if (item.itemTypes == this.item.itemType)
        {
            slot.DetachChildren();
            this.item.transform.position = item.transform.position;
            this.item.transform.rotation = item.transform.rotation;
            controller.RemoveGameObject(item.transform);
            Destroy(item.gameObject);
            pickingUpTime = picikingTime;
            this.item.gameObject.GetComponent<DragableItem>().enabled = false;
            this.item = null;
            controller.CheckIfFInished();

            
        }
    }

}
