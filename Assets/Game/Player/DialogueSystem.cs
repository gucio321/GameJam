using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueSystem : MonoBehaviour
{

    [SerializeField]
    List<string> Dialogi;

    [SerializeField]
    TMP_Text text;

    private bool canWrite = false;
    int currentIndex = 0;


  


    public void ShowDialogue()
    {
        text.text = Dialogi[currentIndex];
        currentIndex++;
    }

    private void Update()
    {
        
    }


}
