using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragableItem : MonoBehaviour
{
    [SerializeField]
    Rigidbody rb;

    public ItemType itemType;



    public Rigidbody Rigidbody => rb;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb.AddRelativeForce(0, 100, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
