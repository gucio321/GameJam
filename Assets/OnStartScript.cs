using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class OnStartScript : MonoBehaviour
{
    [SerializeField]
    Image backgroud;

    [SerializeField]
    DialogueSystem dialogueSystem;

    [SerializeField]
    Canvas canvas;
    [SerializeField]
    AudioClip audioClip;

    float timer = 2f;
    bool WriteDialogue = false;
    int licznik = 0;

    AudioSource audioSource;

    [SerializeField]
    DialogWithOwner owner;

    private void Start()
    {
        canvas.enabled = false;
        audioSource = GetComponent<AudioSource>();
    }
    private void Update()
    {
            if(!audioSource.isPlaying && WriteDialogue == false)
            {
                audioSource.PlayOneShot(audioClip);
                WriteDialogue = true;
            }
            else if(!audioSource.isPlaying && WriteDialogue && timer<0)
            {
                timer = 5;
                canvas.enabled = true;
                dialogueSystem.ShowDialogue();
                licznik++;
            }
            if(licznik == 4)
        {
            owner.work = true;
            Destroy(backgroud);
            gameObject.SetActive(false);
        }
        timer -= Time.deltaTime;
                
    }
        



}
